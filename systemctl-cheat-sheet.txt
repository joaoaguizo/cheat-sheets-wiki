# Check status of a service
sudo systemctl status cdbcluster.service

# Relevant service dirs, can be checked at
man systemd.unit
